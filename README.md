# TAPAdsKit

[![CI Status](https://img.shields.io/travis/shubham14896/TAPAdsKit.svg?style=flat)](https://travis-ci.org/shubham14896/TAPAdsKit)
[![Version](https://img.shields.io/cocoapods/v/TAPAdsKit.svg?style=flat)](https://cocoapods.org/pods/TAPAdsKit)
[![License](https://img.shields.io/cocoapods/l/TAPAdsKit.svg?style=flat)](https://cocoapods.org/pods/TAPAdsKit)
[![Platform](https://img.shields.io/cocoapods/p/TAPAdsKit.svg?style=flat)](https://cocoapods.org/pods/TAPAdsKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TAPAdsKit is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TAPAdsKit'
```

## Author

shubham14896, shubham@tickledmedia.com

## License

TAPAdsKit is available under the MIT license. See the LICENSE file for more info.
